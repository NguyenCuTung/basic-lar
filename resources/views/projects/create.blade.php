@extends('layouts.app')

@section('content')

    <div class="col-md-9 col-lg-9 col-sm-9 pull-left">

        <div class="row col-md-12 col-lg-12 col-sm-12" style="background: white;">
            <h1>Create New Project</h1>
            <form action="{{ route('projects.store') }}" method="post">
                {{ csrf_field() }}


                <div class="form-group">
                    <lable for="project-name">Name <span class="required">*</span></lable>
                    <input placeholder="Enter name"
                           id="project-name"
                           required
                           name="name"
                           spellcheck="false"
                           class="form-control"
                    >
                    <input type="hidden"
                           name="company_id"
                           value="{{ $company_id }}"
                    >
                </div>
                <div class="form-group">
                    <label for="project-content">Description</label>
                    <textarea placeholder="Enter description"
                              id="project-content"
                              required
                              name="description"
                              rows="5" spellcheck="false"
                              class="form-control autosize-target text-left">

                    </textarea>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary pull-right" value="submit">
                </div>
            </form>
        </div>
    </div>

    <div class="col-sm-3 col-md-3 col-lg-3 pull-right">
        <div class="sidebar-module">
            <h4>Action</h4>
            <ol class="list-unstyled">
                <li><a href="/projects">My projects</a></li>

            </ol>
        </div>
    </div>
@endsection